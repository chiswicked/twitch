# Twitch
[![Pipeline Status](https://gitlab.com/chiswicked/twitch/badges/master/pipeline.svg)](https://gitlab.com/chiswicked/twitch/pipelines)
[![Coverage Report](https://gitlab.com/chiswicked/twitch/badges/master/coverage.svg)](https://gitlab.com/chiswicked/twitch/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/chiswicked/twitch)](https://goreportcard.com/report/gitlab.com/chiswicked/twitch)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)
