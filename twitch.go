// Copyright 2019 Norbert Metz
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package twitch

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"
	"golang.org/x/oauth2/twitch"
)

var (
	// ErrUserNotFound is returned when user is not found
	ErrUserNotFound = errors.New("twitch: user not found")
	// ErrClientUninitialized is returned when client object is nil
	// or has no valid Twitch Oauth2 token
	ErrClientUninitialized = errors.New("twitch: client uninitialized")
)

type usersRes struct {
	Data []User `json:"data"`
}

// User struct
type User struct {
	ID              string `json:"id"`
	Login           string `json:"login"`
	DisplayName     string `json:"display_name"`
	Type            string `json:"type,omitempty"`
	BroadcasterType string `json:"broadcaster_type,omitempty"`
	Description     string `json:"description,omitempty"`
	ProfileImageURL string `json:"profile_image_url,omitempty"`
	OfflineImageURL string `json:"offline_image_url,omitempty"`
	ViewCount       int32  `json:"view_count"`
	Email           string `json:"email"`
}

type followersRes struct {
	Total      int        `json:"total"`
	Data       []follows  `json:"data"`
	Pagination Pagination `json:"pagination"`
}

// Pagination struct
type Pagination struct {
	Cursor string `json:"cursor"`
}

type follows struct {
	FromID     string `json:"from_id"`
	FromName   string `json:"from_name"`
	ToID       string `json:"to_id"`
	ToName     string `json:"to_name"`
	FollowedAt string `json:"followed_at"`
}

// Client struct
type Client struct {
	config *clientcredentials.Config
	token  *oauth2.Token
	http   *http.Client
}

// NewClient func
func NewClient(clientID, clientSecret string) (*Client, error) {
	oauth2Config := &clientcredentials.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		TokenURL:     twitch.Endpoint.TokenURL,
	}

	token, err := oauth2Config.Token(context.Background())
	if err != nil {
		return nil, err
	}

	return &Client{
		config: oauth2Config,
		token:  token,
		http:   &http.Client{Timeout: time.Second * 10},
	}, nil
}

// GetUserByName func
func (tc *Client) GetUserByName(name string) (*User, error) {
	return tc.getUserWithQuery("login", name)
}

// GetUserByID func
func (tc *Client) GetUserByID(id string) (*User, error) {
	return tc.getUserWithQuery("id", id)
}
func (tc *Client) getUserWithQuery(key, value string) (*User, error) {
	if tc == nil || tc.token == nil {
		return nil, ErrClientUninitialized
	}
	if len(key) == 0 || len(value) == 0 {
		return nil, errors.New("invalid argument")
	}

	req, err := http.NewRequest("GET", "https://api.twitch.tv/helix/users", nil)
	if err != nil {
		return nil, err
	}

	q := req.URL.Query()
	q.Add(key, value)
	req.URL.RawQuery = q.Encode()

	req.Header.Set("Cache-Control", "no-cache")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Client-ID", tc.config.ClientID)
	req.Header.Set("Authorization", "Bearer "+tc.token.AccessToken)

	resp, err := tc.http.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	ret := &usersRes{}
	err = json.Unmarshal(body, ret)
	if err != nil {
		return nil, err
	}

	if len(ret.Data) < 1 {
		return nil, ErrUserNotFound
	}

	ret2 := &ret.Data[0]
	return ret2, nil
}

// IsFollower func
func (tc *Client) IsFollower(followerID, followedID string) (bool, error) {
	if tc == nil || tc.token == nil {
		return false, ErrClientUninitialized
	}

	// Constructing GET https://api.twitch.tv/helix/users/follows?from_id=<followerID>&to_id=<followedID>
	req, err := http.NewRequest("GET", "https://api.twitch.tv/helix/users/follows", nil)
	if err != nil {
		return false, err
	}

	q := req.URL.Query()
	q.Add("from_id", followerID)
	q.Add("to_id", followedID)
	req.URL.RawQuery = q.Encode()

	req.Header.Set("Cache-Control", "no-cache")
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Client-ID", tc.config.ClientID)
	req.Header.Set("Authorization", "Bearer "+tc.token.AccessToken)

	resp, err := tc.http.Do(req)
	if err != nil {
		return false, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return false, err
	}

	ret := &followersRes{}
	err = json.Unmarshal(body, ret)
	if err != nil {
		return false, err
	}

	fmt.Println(ret)
	if ret.Total != 1 {
		return false, nil
	}

	return true, nil
}
